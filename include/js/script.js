(function() {
  /*
   * Drawing the color wheel.
   * Get the canvas where we can draw the color wheel.
   */
  var canvas = document.getElementById('colorSelectorCanvas');
  var ctx = canvas.getContext('2d');
  var pickerMode = 0;
  var colorMode = {
    HSL:  0,
    RGB:  1,
    LAB:  2,
    CMYK: 3,
    HEX:  4,
    HSV:  5,

    hsl:  0,
    rgb:  1,
    lab:  2,
    smyk: 3,
    hex:  4,
    hsv:  5
  };

  /* Object for color picker slider */
  var slider = {
    slider: document.getElementById('slider'),
    track: document.getElementById('slider-track'),
    knobParent: document.getElementById('slider-knob'),
    knobShape: document.getElementById('slider-knob-shape')
  };

  /* Object reference for the picker */
  var picker = {
    pointer: document.getElementById('pointer'),
    canvas:  canvas,
    container: document.getElementById('colorPicker')
  };

  /* Color object constructor */
  class Color{
    constructor(m, p){
      this.mode = m;
      this.color = p;
    }
  }

  var selectedColor = new Color(colorMode.hsl, [0,0,0]);
  switchMode(pickerMode); // Draw default picker

  /* Add events for the slider */
  applyEvent_slider();

  /* Add event listener for the picker mode */
  applyEvent_pickerMode();

  /* Add event listener for color picker */
  applyEvent_colorPicker();

  function drawPicker(mode){
    /*
     * A function to draw color picker. This picker should support
     * all selection mode.
     */

    switch (mode) {
      case 0: hueDraw(); break;
      case 1: saturationDraw(); break;

      default: // Draw using the default mode (hue)
      hueDraw();
    }
  }

  function drawSlider(mode){
    /*
     * A function to draw color slider. This slider should support
     * all selection mode.
     */

    switch (mode) {
      case 0: hueSlider(); break;
      case 1: saturationSlider(); break;

      default: // Draw using the default mode (hue)
      hueSlider();
    }
  }

  function switchMode(mode){
    /*
     * A function that aids the color selection mode
     */

    mode = Number.parseInt(mode);
    this.pickerMode = mode;

    /* Force color update */
    forceColorUpdate();

    /* Redraw the slider & picker */
    drawPicker(mode);
    drawSlider(mode);
  }

  function pickerRedraw(){
    drawPicker(this.pickerMode);
  }

  function sliderRedraw(){
    drawSlider(this.pickerMode);
  }

  function forceColorUpdate(){
    /* A function that forces selected color update */

    /* Get slider and picker location */
    var slider_width = slider.track.offsetWidth;
    var picker_size  = {w: picker.container.offsetWidth, h: picker.container.offsetHeight};

    var slider_value = Number.parseInt(slider.knobParent.style.left) / slider_width * 100;
    var picker_value = {x: Number.parseInt(picker.pointer.style.left) / picker_size.w * 100, y: Number.parseInt(picker.pointer.style.top) / picker_size.h * 100};

    slider_value = (slider.knobParent.style.left == "" ? 0 : slider_value);
    picker_value.x = (picker.pointer.style.left == "" ? 0 : picker_value.x);
    picker_value.y = (picker.pointer.style.top == "" ? 0 : picker_value.y);

    /* Update the color */
    colorUpdate(slider_value, true);
    colorUpdate({x: picker_value.x, y: picker_value.y * 100}, false);
  }

  function colorUpdate(value, isSlider){
    switch (this.pickerMode) {
      case 0: // Hue slider
        if(isSlider){
          selectedColor.color[0] = (value / 100) * 360;
        }else{
          var hsl_color = colorConversion(new Color(colorMode.HSV, [selectedColor.color[0], value.x, 100 - value.y]), colorMode.HSL);

          selectedColor.color[1] = hsl_color.color[1];
          selectedColor.color[2] = hsl_color.color[2];
        }
        break;

      case 1: // Saturation slider
        if(isSlider){
          selectedColor.color[1] = value;
        }else{
          selectedColor.color[0] = value.x / 100 * 360;
          selectedColor.color[2] = (((2 - (selectedColor.color[1] / 100)) * (Math.abs(value.y - 100) / 100)) / 2) * 100;
        }
        break;

      case 2: // Lighness slider
        if(isSlider){
          selectedColor.color[2] = value;
        }else{
          var hsl_color = colorConversion(new Color(colorMode.HSV, [selectedColor.color[0], value.x, 100 - value.y]), colorMode.HSL);

          selectedColor.color[0] = hsl_color.color[0];
          selectedColor.color[1] = hsl_color.color[1];
        }
        break;
      default:

    }
  }

  /*
   * Mode changer functions
   */

  var drawMode = {
    H:  0,
    S:  1,
    L:  2,
    R: 3,
    G:  4,
    B:  5,

    h:  0,
    s:  1,
    l:  2,
    r: 3,
    g: 4,
    b: 5
  };

  function hueDraw(){
    for(var i = 1; i <= canvas.height; i++){
      var grd = ctx.createLinearGradient(0, i, canvas.width, i);

      var brightness = (canvas.height - (i - 1)) / canvas.height * 100;

      for(var j = 0; j <= 100; j++){
        var gradient = colorConversion(new Color(colorMode.HSV, [selectedColor.color[0], j, brightness]), colorMode.HSL);
        gradient = gradient.color;

        // console.log(gradient[0]);

        try{
          grd.addColorStop(j / 100,"hsl("+ gradient[0] +","+ gradient[1] +"%,"+ gradient[2] +"%)");
        }catch (e){
          continue;
        }
      }

      ctx.fillStyle = grd;
      ctx.fillRect(0,i,canvas.width,1);
    }
  }

  function saturationDraw(){
    for(var i = 1; i <= canvas.height; i++){
      var grd = ctx.createLinearGradient(0, i, canvas.width, i);

      var brightness = ((Math.abs(i - canvas.height) / canvas.height) * 100);

      for(var j = 0; j <= 360; j++){
        var gradient = colorConversion(new Color(colorMode.HSV, [j, selectedColor.color[1], brightness]), colorMode.HSL);
        try{
          grd.addColorStop(j / 360,"hsl("+ j +","+ selectedColor.color[1] +"%,"+ gradient.color[2] +"%)");
        }catch (e){
          continue;
        }
      }

      ctx.fillStyle = grd;
      ctx.fillRect(0,i,canvas.width,1);
    }
  }

  function hueSlider(){
    slider.track.style.background = 'linear-gradient(to right,hsl(0,100%,50%),hsl(60,100%,50%),hsl(120,100%,50%),hsl(180,100%,50%),hsl(240,100%,50%),hsl(300,100%,50%),hsl(360,100%,50%))';
  }

  function saturationSlider(){
    slider.track.style.background = 'linear-gradient(to right,hsl('+selectedColor.color[0]+','+100+'%,'+0+'%),hsl('+selectedColor.color[0]+','+100+'%,'+selectedColor.color[2]+'%))';
  }

  function applyEvent_slider(){
    slider.slider.addEventListener('mousedown', function(e){
      e.preventDefault();
      /* Get new position */
      var newLocation = e.pageX - this.getBoundingClientRect().left;

      /* Apply new location */
      slider.knobParent.style.left = newLocation + 'px';

      slider.slider.dataset.active = 'true';

      /* Update the selected color */
      colorUpdate((newLocation / slider.slider.offsetWidth) * 100, true);

      /* Redraw the color picker */
      pickerRedraw();

      /* Update the UI */
      UIUpdate();
    });

    slider.slider.addEventListener('mouseup', function(e){
      slider.slider.dataset.active = 'false';
    });

    slider.slider.addEventListener('mouseleave', function(e){
      slider.slider.dataset.active = 'false';
    });

    slider.slider.addEventListener('mousemove', function(e){
      if(slider.slider.dataset.active == 'true'){
        e.preventDefault();
        /* Get new position */
        var newLocation = e.pageX - this.getBoundingClientRect().left;

        /* Slider limiter */
        newLocation = (newLocation > slider.slider.offsetWidth ? slider.slider.offsetWidth : newLocation); // Block at right
        newLocation = (newLocation < 0 ? 0 : newLocation); // Block at left

        /* Apply new location */
        slider.knobParent.style.left = newLocation + 'px';

        /* Update the selected color */
        colorUpdate((newLocation / slider.slider.offsetWidth) * 100, true);

        /* Redraw the color picker */
        pickerRedraw();

        /* Update the UI */
        UIUpdate();
      }
    });

    slider.slider.addEventListener('touchend', function(e){
      /* Get new position */
      var newLocation = e.pageX - this.getBoundingClientRect().left;

      /* Apply new location */
      slider.knobParent.style.left = newLocation + 'px';

      /* Update the selected color */
      colorUpdate(newLocation / slider.slider.offsetWidth * 100, true);

      /* Redraw the color picker */
      pickerRedraw();

      /* Update the UI */
      UIUpdate();
    });

    slider.slider.addEventListener('touchmove', function(e){
      e.preventDefault();

      /* Get new position */
      var newLocation = e.pageX - this.getBoundingClientRect().left;

      /* Slider limiter */
      newLocation = (newLocation > slider.slider.offsetWidth ? slider.slider.offsetWidth : newLocation); // Block at right
      newLocation = (newLocation < 0 ? 0 : newLocation); // Block at left

      /* Apply new location */
      slider.knobParent.style.left = newLocation + 'px';

      /* Update the UI */
      UIUpdate();
    });
  }

  function applyEvent_pickerMode(){
    /* Get list of mode option */
    var modePicker = document.getElementsByClassName('selectModeChanger');

    /* Register event listener to change the active mode */
    for(var i = 0; i < modePicker.length; i++){
      var that = this;
      modePicker[i].addEventListener('change', function(e){
        that.pickerMode = Number.parseInt(this.value);
        switchMode(this.value);
      });
    }
  }

  function applyEvent_colorPicker(){
    picker.container.addEventListener('mousedown', function(e){
      e.preventDefault();

      /* Calculate new position */
      var newLocation = {x: e.pageX - this.getBoundingClientRect().left, y: e.pageY - this.getBoundingClientRect().top};

      /* Limit edge of the picker */
      newLocation.x = (newLocation.x < 0 ? 0 : newLocation.x);
      newLocation.x = (newLocation.x > canvas.offsetWidth ? canvas.offsetWidth : newLocation.x);

      newLocation.y = (newLocation.y < 0 ? 0 : newLocation.y);
      newLocation.y = (newLocation.y > canvas.offsetHeight ? canvas.offsetHeight : newLocation.y);

      picker.container.dataset.active = 'true';

      /* Set new location */
      picker.pointer.style.top = newLocation.y + 'px';
      picker.pointer.style.left = newLocation.x + 'px';

      /* Update the selected color */
      colorUpdate({x: (newLocation.x / picker.container.offsetWidth) * 100, y: (newLocation.y / picker.container.offsetHeight) * 100}, false);

      /* Redraw the color slider */
      sliderRedraw();

      /* Update the UI */
      UIUpdate();
    });

    picker.container.addEventListener('mouseup', function(e){
      picker.container.dataset.active = 'false';
    });

    picker.container.addEventListener('mouseleave', function(e){
      picker.container.dataset.active = 'false';
    });

    picker.container.addEventListener('mousemove', function(e){

      if(picker.container.dataset.active == 'true'){
        e.preventDefault();

        /* Calculate new position */
        var newLocation = {x: e.pageX - this.getBoundingClientRect().left, y: e.pageY - this.getBoundingClientRect().top};

        /* Limit edge of the picker */
        newLocation.x = (newLocation.x < 0 ? 0 : newLocation.x);
        newLocation.x = (newLocation.x > canvas.offsetWidth ? canvas.offsetWidth : newLocation.x);

        newLocation.y = (newLocation.y < 0 ? 0 : newLocation.y);
        newLocation.y = (newLocation.y > canvas.offsetHeight ? canvas.offsetHeight : newLocation.y);

        /* Set new location */
        picker.pointer.style.top = newLocation.y + 'px';
        picker.pointer.style.left = newLocation.x + 'px';

        /* Update the selected color */
        colorUpdate({x: (newLocation.x / picker.container.offsetWidth) * 100, y: (newLocation.y / picker.container.offsetHeight) * 100}, false);

        /* Redraw the color slider */
        sliderRedraw();

        /* Update the UI */
        UIUpdate();
      }
    });

    picker.container.addEventListener('touchend', function(e){
      e.preventDefault();

      /* Calculate new position */
      var newLocation = {x: e.pageX - this.getBoundingClientRect().left, y: e.pageY - this.getBoundingClientRect().top};

      /* Limit edge of the picker */
      newLocation.x = (newLocation.x < 0 ? 0 : newLocation.x);
      newLocation.x = (newLocation.x > canvas.offsetWidth ? canvas.offsetWidth : newLocation.x);

      newLocation.y = (newLocation.y < 0 ? 0 : newLocation.y);
      newLocation.y = (newLocation.y > canvas.offsetHeight ? canvas.offsetHeight : newLocation.y);

      picker.container.dataset.active = 'true';

      /* Set new location */
      picker.pointer.style.top = newLocation.y + 'px';
      picker.pointer.style.left = newLocation.x + 'px';

      /* Update the selected color */
      colorUpdate({x: (newLocation.x / picker.container.offsetWidth) * 100, y: (newLocation.y / picker.container.offsetHeight) * 100}, false);

      /* Redraw the color slider */
      sliderRedraw();

      /* Update the UI */
      UIUpdate();
    });

    picker.container.addEventListener('touchmove', function(e){

      if(picker.container.dataset.active == 'true'){
        e.preventDefault();

        console.log(e.pageX);

        /* Calculate new position */
        var newLocation = {x: e.pageX - this.getBoundingClientRect().left, y: e.pageY - this.getBoundingClientRect().top};

        /* Limit edge of the picker */
        newLocation.x = (newLocation.x < 0 ? 0 : newLocation.x);
        newLocation.x = (newLocation.x > canvas.offsetWidth ? canvas.offsetWidth : newLocation.x);

        newLocation.y = (newLocation.y < 0 ? 0 : newLocation.y);
        newLocation.y = (newLocation.y > canvas.offsetHeight ? canvas.offsetHeight : newLocation.y);

        /* Set new location */
        picker.pointer.style.top = newLocation.y + 'px';
        picker.pointer.style.left = newLocation.x + 'px';

        /* Update the UI */
        UIUpdate();
      }
    });
  }

  function UIUpdate(){
    var input = {
      hsl: {
        h: document.getElementById('hue'),
        s: document.getElementById('saturation'),
        l: document.getElementById('lighness')
      },
      rgb: {
        r: document.getElementById('red'),
        g: document.getElementById('green'),
        b: document.getElementById('blue')
      },
      lab: {
        l: document.getElementById('L'),
        a: document.getElementById('a'),
        b: document.getElementById('b')
      },
      cmyk: {
        c: document.getElementById('cyan'),
        m: document.getElementById('magenta'),
        y: document.getElementById('yellow'),
        k: document.getElementById('key')
      },
      hex: document.getElementById('hex'),
      preview: document.getElementById('color-preview')
    }

    /* Display HSL */
    input.hsl.h.value = Math.round(selectedColor.color[0]);
    input.hsl.s.value = Math.round(selectedColor.color[1]);
    input.hsl.l.value = Math.round(selectedColor.color[2]);

    /* Display RGB */
    var color_rgb = colorConversion(selectedColor, colorMode.RGB);
    input.rgb.r.value = Math.round(color_rgb.color[0]);
    input.rgb.g.value = Math.round(color_rgb.color[1]);
    input.rgb.b.value = Math.round(color_rgb.color[2]);

    /* Display Hex */
    var color_hex = colorConversion(selectedColor, colorMode.HEX);
    input.hex.value = color_hex.color;

    /* Set color preview */
    input.preview.style.background = '#'+color_hex.color;
  }

  /*
   * Auxilary functions
   */

  function polarToCartesian(r, t){
    /* Convert polar to cartesian */
    var x = r * Math.cos(t);
    var y = r * Math.sin(t);

    return {x: x, y: y};
  }

  function colorConversion(color, target){
    /*
     * A function to convert any color
     */

    if(color.mode == target)
    return color;

    var hsl = null;

    /* Convert all color to HSV first */
    switch (color.mode) {
      case colorMode.RGB:
        var rgb_col = new Color(colorMode.RGB, [color.color[0] / 255, color.color[1] / 255, color.color[2] / 255]);

        var Cmax  = Math.max(rgb_col.color[0], rgb_col.color[1], rgb_col.color[2]);
        var Cmin  = Math.min(rgb_col.color[0], rgb_col.color[1], rgb_col.color[2]);
        var delta = Cmax - Cmin;

        var hue, sat, lig;

        /* Calculate the hue */
        if(delta == 0){hue = 0;}
        else if(Cmax == rgb_col.color[0]){
          var over = rgb_col.color[1] - rgb_col.color[2]; // G' - B'
          var fraction = over / delta; // (G' - B') / delta
          hue = 60 * (fraction % 6); // 60 * (((G' - B') / delta) % 6)
        }else if(Cmax == rgb_col.color[1]){
          var over = rgb_col.color[2] - rgb_col.color[0]; // B' - R'
          var fraction = over / delta; // (B' - R') / delta
          hue = 60 * (fraction + 2); // 60 * (((B' - R') / delta + 2)
        }else if(Cmax == rgb_col.color[2]){
          var over = rgb_col.color[0] - rgb_col.color[1]; // R' - G'
          var fraction = over / delta; // (R' - G') / delta
          hue = 60 * (fraction + 4); // 60 * (((R' - G') / delta) + 4)
        }

        /* Calculate the lightness */
        lig = (Cmax + Cmin) / 2;

        /* Calculate the saturation */
        if(Cmax == 0){sat = 0}
        else{
          if((1 - Math.abs(2 * lig - 1)) == 0)
            sat = 0;
          else
            sat = delta / (1 - Math.abs(2 * lig - 1));
        }

        hue = (hue < 0 ? 360 + hue : hue);
        hsl = new Color(colorMode.HSL, [hue, sat * 100, lig * 100]);
        break;

      case colorMode.HSV:
        var hue, h, s, v;

        /* Shorten the variable */
        hue = h = color.color[0];
        s = color.color[1] / 100;
        v = color.color[2] / 100;

        var sv = s*v;
        h = (2-s) * v;
        var logic = (h < 1 ? h : 2 - h);
        logic = (logic == 0 ? 1 : logic);
        s = sv / logic;

        hsl = new Color(colorMode.HSL, [hue, s * 100, (h / 2) * 100]);
        break;

      case colorMode.HSL:
        hsl = new Color(color.mode, Array.from(color.color));
        break;

      case colorMode.HEX:
        /* Initialize hex input variable */
        var hex = color.color;

        /* Initialize RGB variable */
        var color_rgb = null;

        /* Initialize RGB component variable */
        var r, g, b;

        /*
         * Check if the hex is in shorthand form.
         * If hex is in shorthand form, convert to long form.
         */
        if(hex.length == 3 || (hex.length == 4 && hex[0] == '#')){
            var shorthandRegex = /^#?([a-f\d])([a-f\d])([a-f\d])$/i;
            hex = hex.replace(shorthandRegex, function(m, r, g, b) {
                return r + r + g + g + b + b;
            }).toUpperCase();
        }

        var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);

        /* Fill out the component variable */
        r = parseInt(result[1], 16);
        g = parseInt(result[2], 16);
        b = parseInt(result[3], 16);

        color_rgb = new Color(colorMode.RGB, [r,g,b]);

        /* Convert RGB to HSL for to comply with standard */
        hsl = colorConversion(color_rgb, colorMode.HSL);

        break;
    }

    /* Convert HSL to target mode */
    switch (target) {
      case colorMode.HSL: return hsl;
      case colorMode.RGB:
        hsl.color[1] /= 100;
        hsl.color[2] /= 100;

        hsl.color[0] = (hsl.color[0] == 360 ? 0 : hsl.color[0]);

        var C = (1 - Math.abs(2 * hsl.color[2] - 1)) * hsl.color[1];
        var X = C * (1 - Math.abs((hsl.color[0] / 60) % 2 - 1));
        var m = hsl.color[2] - C/2;

        var color = null;

        if(hsl.color[0] >= 0 && hsl.color[0] < 60)
          color = new Color(colorMode.RGB, [C,X,0]);
        else if(hsl.color[0] >= 60 && hsl.color[0] < 120)
          color = new Color(colorMode.RGB, [X,C,0]);
        else if(hsl.color[0] >= 120 && hsl.color[0] < 180)
          color = new Color(colorMode.RGB, [0,C,X]);
        else if(hsl.color[0] >= 180 && hsl.color[0] < 240)
          color = new Color(colorMode.RGB, [0,X,C]);
        else if(hsl.color[0] >= 240 && hsl.color[0] < 300)
          color = new Color(colorMode.RGB, [X,0,C]);
        else if(hsl.color[0] >= 300 && hsl.color[0] < 360)
          color = new Color(colorMode.RGB, [C,0,X]);

        color = color.color;

        color = new Color(colorMode.RGB, [(color[0]+m)*255, (color[1]+m)*255, (color[2]+m)*255]);
        break;

      case colorMode.HEX:
        /* Convert HSL to RGB because Hex code is in RGB */
        var rgb_color = colorConversion(hsl, colorMode.RGB);
        rgb_color = rgb_color.color;

        /* Convert to binary */
        var bin = rgb_color[0] << 16 | rgb_color[1] << 8 | rgb_color[2];

        color = new Color(colorMode.HEX, bin.toString(16).toUpperCase());

        break;
      default:

    }

    return color;
  }

  function degreeToRadian(d){return d * Math.PI/180;}
  function radianToDegree(r){return r * 180/Math.PI;}


  /*
   * Expose inportant functions to the global scope
   */
  window.degreeToRadian = degreeToRadian;
  window.radianToDegree = radianToDegree;

  /* Expose Color object to global */
  window.Color = Color;

  /* Define available color modes for easy selection */
  window.colorMode = colorMode;

  /* Define available draw modes for easy selection */
  window.DrawMode = drawMode;

  window.switchMode = switchMode;
  window.colorConversion = colorConversion;
  window.pickerMode = pickerMode;
})();
